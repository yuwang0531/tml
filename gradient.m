function [dLu,dLm,dLa,dLb,dLc,dLd] = gradient(P,ALu,ALm,R_bar,lrn_ratings,u2ml,m2ul,N,Nu,Nm,N_Cu,N_Cm,K,L)

rdu = zeros(L+N_Cu+N_Cm,K,N);
rdm = zeros(L+N_Cu+N_Cm,K,N);

for ii = 1:N
    temp = (R_bar(ii) - lrn_ratings(ii))*((1:K)-R_bar(ii))'.*P(:,ii);
    for jj = 1:K
        rdu(:,jj,ii) = temp(jj) .* ALm(:,jj,ii);
        rdm(:,jj,ii) = temp(jj) .* ALu(:,jj,ii);
    end
end

dLu = zeros(L,K,Nu);
dLa = zeros(N_Cm,K,Nu);
for ii = 1:Nu
    dLu(:,:,ii) = sum(rdu(1:L,:,u2ml{ii}),3);
    dLa(:,:,ii) = sum(rdu(L+N_Cu+1:end,:,u2ml{ii}),3);
end

dLm = zeros(L,K,Nm);
dLb = zeros(N_Cu,K,Nm);
for ii = 1:Nm
    dLm(:,:,ii) = sum(rdm(1:L,:,m2ul{ii}),3);
    dLb(:,:,ii) = sum(rdu(L+1:L+N_Cu,:,m2ul{ii}),3);
end

dLc(:,:) = sum(rdu,3);
dLd(:,:) = sum(rdu,3);

end

