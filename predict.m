function [P,R_bar,rmse] = predict(ALu,ALm,N,K,ratings)

P(:,:) = sum(ALu(:,:,:).*ALm(:,:,:),1);
P = exp(P);
for ii = 1:N
    P(:,ii) = P(:,ii)/sum(P(:,ii));
end
R_bar = ((1:K) * P)'; % prediction of ratings
rmse = rms(ratings - R_bar); % rmse

end

