%%%%%%%%% gradient descend to minimize variance %%%%%%%%%%
clc; clear;
filename = '1M';
load(strcat(filename,'.mat'))
rng(531)
K = 5; % number of ratings
L = 1; % number of latent factors
N_iter = 300; % number of iterations
N_test = N - N_train - N_tune;
rmse_train = zeros(N_iter,1);
rmse_tune = zeros(N_iter,1);
rmse_test = zeros(N_iter,1);
counter = 1;


%%%%%%%%% covariant information %%%%%%%%%%
min_gender = min(users_gender);
min_age = min(users_age);
min_occupation = min(users_occupation);
min_zip = min(users_zip);
Cu_gender = zeros(max(users_gender)-min_gender,Nu);
Cu_age = zeros(max(users_age)-min_age,Nu);
Cu_occupation = zeros(max(users_occupation)-min_occupation,Nu);
Cu_zip = zeros(max(users_zip)-min_zip,Nu);
for ii = 1:Nu
    if users_gender(ii) ~= min_gender
        Cu_gender(users_gender(ii)-min_gender,ii) = 1;
    end
    if users_age(ii) ~= min_age
        Cu_age(users_age(ii)-min_age,ii) = 1;
    end
    if users_occupation(ii) ~= min_occupation
        Cu_occupation(users_occupation(ii)-min_occupation,ii) = 1;
    end
    if users_zip(ii) ~= min_zip
        Cu_zip(users_zip(ii)-min_zip,ii) = 1;
    end
end
Cu_all = [Cu_gender;Cu_age;Cu_occupation;Cu_zip];
N_Cu = size(Cu_all,1);
Cu = zeros(N_Cu,K,Nu);
for ii = 1:N_Cu
    for jj = 1:K
        for kk = 1:Nu
            Cu(ii,jj,kk) = Cu_all(ii,kk);
        end
    end
end
Cm_all = movies_genre';
N_Cm = size(Cm_all,1);
Cm = zeros(N_Cm,K,Nu);
for ii = 1:N_Cm
    for jj = 1:K
        for kk = 1:Nm
            Cm(ii,jj,kk) = Cm_all(ii,kk);
        end
    end
end



%%%%%%%%% start iteration %%%%%%%%%%
Lu = zeros(L,K,Nu);
Lm = zeros(L,K,Nm);
for ii = 1:Nu
    if any(lrn_users == ii)
        Lu(:,:,ii) = (1 - 2*rand(L,K))/10; % users latent factor matrix
    end
end
for ii = 1:Nm
    if any(lrn_movies == ii)
        Lm(:,:,ii) = (1 - 2*rand(L,K))/10; % movies latent factor matrix
    end
end
La = (1 - 2*rand(N_Cm,K,Nu))/10;
Lb = (1 - 2*rand(N_Cu,K,Nm))/10;
Lc = ones(L+N_Cu+N_Cm,1)*sqrt(log(hist(lrn_ratings,K))/(L+N_Cu+N_Cm));
Ld = Lc;
ALu = zeros(L+N_Cu+N_Cm,K,N_train);
ALm = zeros(L+N_Cu+N_Cm,K,N_train);
for jj = 1:N_train
    ALu(:,:,jj) = [Lu(:,:,lrn_users(jj));Cu(:,:,lrn_users(jj));La(:,:,lrn_users(jj))] + Lc; % augmented users latent factor matrix
    ALm(:,:,jj) = [Lm(:,:,lrn_movies(jj));Lb(:,:,lrn_movies(jj));Cm(:,:,lrn_movies(jj))] + Ld; % augmented movies latent factor matrix
end
[P,R_bar,rmse] = predict(ALu,ALm,N_train,K,lrn_ratings);

for ii = 1:N_iter
    [dLu,dLm,dLa,dLb,dLc,dLd] = gradient(P,ALu,ALm,R_bar,lrn_ratings,u2ml,m2ul,N_train,Nu,Nm,N_Cu,N_Cm,K,L); % gradient    
    s1 = exprnd(1/max(max(max(abs(dLu))))/counter^2/30);
    s2 = exprnd(1/max(max(max(abs(dLm))))/counter^2/30);
    s3 = exprnd(1/max(max(max(abs(dLa))))/counter^2/30);
    s4 = exprnd(1/max(max(max(abs(dLb))))/counter^2/30);
    s5 = exprnd(1/max(max(max(abs(dLc))))/counter^2/30);
    s6 = exprnd(1/max(max(max(abs(dLd))))/counter^2/30);
    Lu_new = Lu - dLu * s1;
    Lm_new = Lm - dLm * s2;
    La_new = La - dLa * s3;
    Lb_new = Lb - dLb * s4;
    Lc_new = Lc - dLc * s5;
    Ld_new = Ld - dLd * s6;
    ALu_new = zeros(L+N_Cu+N_Cm,K,N_train);
    ALm_new = zeros(L+N_Cu+N_Cm,K,N_train);
    for jj = 1:N_train
        ALu_new(:,:,jj) = [Lu_new(:,:,lrn_users(jj));Cu(:,:,lrn_users(jj));La_new(:,:,lrn_users(jj))] + Lc_new; % augmented users latent factor matrix
        ALm_new(:,:,jj) = [Lm_new(:,:,lrn_movies(jj));Lb_new(:,:,lrn_movies(jj));Cm(:,:,lrn_movies(jj))] + Ld_new; % augmented movies latent factor matrix
    end
    [P_new,R_bar_new,rmse_new] = predict(ALu_new,ALm_new,N_train,K,lrn_ratings);
    if rmse > rmse_new % descend if total square error increases
        Lu = Lu_new;
        Lm = Lm_new;
        La = La_new;
        Lb = Lb_new;
        Lc = Lc_new;
        Ld = Ld_new;
        ALu = ALu_new;
        ALm = ALm_new;
        P = P_new;
        R_bar = R_bar_new;
        rmse = rmse_new;
        counter = 1;
    else
        counter = counter + 1;
    end
    rmse_train(ii) = rmse;

    %%%%%%%%%%%%%%%%% Tuning RMSE %%%%%%%%%%%%%%%%%%%%%%%
    ALu_tune = zeros(L+N_Cu+N_Cm,K,N_tune);
    ALm_tune = zeros(L+N_Cu+N_Cm,K,N_tune);
    for jj = 1:N_tune
        ALu_tune(:,:,jj) = [Lu(:,:,tune_users(jj));Cu(:,:,tune_users(jj));La(:,:,tune_users(jj))] + Lc; % augmented users latent factor matrix
        ALm_tune(:,:,jj) = [Lm(:,:,tune_movies(jj));Lb(:,:,tune_movies(jj));Cm(:,:,tune_movies(jj))] + Ld; % augmented movies latent factor matrix
    end
    [~,~,rmse_tune(ii)] = predict(ALu_tune,ALm_tune,N_tune,K,tune_ratings);

    %%%%%%%%%%%%%%%%% Test RMSE %%%%%%%%%%%%%%%%%%%%%%%
    ALu_test = zeros(L+N_Cu+N_Cm,K,N_test);
    ALm_test = zeros(L+N_Cu+N_Cm,K,N_test);
    for jj = 1:N_test
        ALu_test(:,:,jj) = [Lu(:,:,test_users(jj));Cu(:,:,test_users(jj));La(:,:,test_users(jj))] + Lc; % augmented users latent factor matrix
        ALm_test(:,:,jj) = [Lm(:,:,test_movies(jj));Lb(:,:,test_movies(jj));Cm(:,:,test_movies(jj))] + Ld; % augmented movies latent factor matrix
    end   
    [~,~,rmse_test(ii)] = predict(ALu_test,ALm_test,N_test,K,test_ratings);
end

save(strcat(filename,'_',num2str(L),'_',num2str(N_iter),'_rand_new.mat'));
min(rmse_test)