%%%%%%%%%%% Post-processing %%%%%%%%%%%%%%%%

filename = 'ratings_1M';
% lambda = [0, 0.0001, 0.04, 0.1, 0.4, 1, 4, 10, 40]';
% lambda = 0.1; 
% for i = 1:length(lambda)
%     lambda = [0, 0.0001, 0.04, 0.1, 0.4, 1, 4, 10, 40]';
%     load(strcat(filename,'_200_',num2str(lambda(i)),'.mat'))
%     [~, index] = min(rmse_tune(1:N_iter));
%     fprintf('%d, %f, %f\n', index, lambda, rmse_test(index));
% end
load(strcat(filename,'_200_10.mat'))
[~, index] = min(rmse_tune(1:N_iter));
fprintf('%d, %f, %f\n', index, lambda, rmse_test(index));

figure(1)
hold on
% plot(1:70,rmse_train(1:70),'-')
plot(1:N_iter,rmse_tune,'-')
plot(1:N_iter,rmse_test,'--')
legend('Tuning','Testing')
xlabel('Iteration')
ylabel('RMSE')