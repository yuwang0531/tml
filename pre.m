%% Pre-processing data
clc
clear



%%%%%%%%%%%%% read data %%%%%%%%%%%%%
fid = fopen('ratings.dat');
rawdata = textscan(fid,'%d %d %f %d','Delimiter',':','MultipleDelimsAsOne',1);
fclose(fid);
[~,timeorder] = sort(rawdata{4});
all_users = rawdata{1};
all_movies = rawdata{2};
all_ratings = rawdata{3};
all_users = all_users(timeorder);
all_movies = all_movies(timeorder);
all_ratings = double(all_ratings(timeorder));
N = length(all_ratings);
Nu = max(all_users);
Nm = max(all_movies);

fid = fopen('users.dat');
rawdata = textscan(fid,'%d %s %d %d %s','Delimiter',':','MultipleDelimsAsOne',1);
fclose(fid);
users_gender = int32(zeros(Nu,1));
for ii = 1:length(rawdata{2})
    if rawdata{2}{ii} == 'F'
       users_gender(ii) = 1;
    end
end
users_age = rawdata{3};
users_age = users_age/10 + ones();
N_age = max(users_age);
users_occupation = rawdata{4};
users_occupation = users_occupation + ones();
users_zip = str2double(strrep(rawdata{5},'-','.'));
users_zip = int32(ceil(users_zip/10000));
users_zip(users_zip > 10) = 0;

genre = {'Action','Adventure','Animation','Children''s','Comedy','Crime','Documentary','Drama','Fantasy','Film-Noir','Horror','Musical','Mystery','Romance','Sci-Fi','Thriller','War','Western'};
N_genre = length(genre);
movies_genre = zeros(Nm,N_genre);
fid = fopen('movies.dat');
while ~feof(fid)
    line = strsplit(fgets(fid),'::');
    for jj = 1:N_genre
        if ~isempty(strfind(line{end},genre{jj}))
            movies_genre(str2double(line{1}),jj) = 1;
        end
    end
end
fclose(fid);


%%%%%%%%%%%% training data %%%%%%%%%%%%
train_ratio = 0.6;
tune_ratio = 0.15;
N_train = floor(N * train_ratio);
N_tune = floor(N * tune_ratio);

lrn_users = all_users(1:N_train,:); % training data
lrn_movies = all_movies(1:N_train,:);
lrn_ratings = all_ratings(1:N_train,:);

tune_users = all_users(N_train+1:N_train+N_tune,:); % tuning data
tune_movies = all_movies(N_train+1:N_train+N_tune,:);
tune_ratings = all_ratings(N_train+1:N_train+N_tune,:);

test_users = all_users(N_train+N_tune+1:N,:); % testing data
test_movies = all_movies(N_train+N_tune+1:N,:);
test_ratings = all_ratings(N_train+N_tune+1:N,:);



%%%%%%%%%%%% construct maps %%%%%%%%%%%%
u2ml = cell(Nu,1); % map from a user to rating movies
m2ul = cell(Nm,1); % map from a movie to rated users
for ii = 1:Nu
    u2ml{ii} = find(lrn_users == ii);
end
for jj = 1:Nm
    m2ul{jj} = find(lrn_movies == jj);
end



%%%%%%%%%% save data %%%%%%%%%%%%%
dlmwrite('train',[lrn_users, lrn_movies, lrn_ratings]);
dlmwrite('tune',[tune_users, tune_movies, tune_ratings]);
dlmwrite('test',[test_users, test_movies, test_ratings]);
save('1M.mat')